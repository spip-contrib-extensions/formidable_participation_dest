# CHANGELOG
## 3.1.0 - 2024-10-03
### Removed
- Compatibilité SPIP < 4.2

## 3.0.4 - 2024-06-09

### Fixed

- Compatible SPIP 4++
## 3.0.3 - 2023-03-18

### Added

- Compatibilité SPIP 4.2

## 3.0.2 - 2022-07-05

### Added

- Compatibilité formidable 5.2 (qui sérialize autrement les saisies et traitements)

## 3.0.1 - 2022-05-31

### Added

- Ajout d'une classe `.select2` sur la liste des destinataires potentiel·les
- Compatibilité SPIP 4

### Changed

- Logo à la racine du plugin

### Removed

- Compatiblité SPIP 3.2
