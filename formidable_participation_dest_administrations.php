<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/cextras');
include_spip('base/formidable_participation_dest');
include_spip('base/upgrade');
function formidable_participation_dest_upgrade($nom_meta_base_version, $version_cible) {

	$maj = [];
	cextras_api_upgrade(formidable_participation_dest_declarer_champs_extras(), $maj['create']);
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function formidable_participation_dest_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(formidable_participation_dest_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}
