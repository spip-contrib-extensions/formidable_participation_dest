<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Lors du traitement email d'un formulaire
 * cherche les emails associés à l'évènement, et les ajoute aux destinataires
 * @param $flux
 * @return $flux
 **/
function formidable_participation_dest_formidable_traiter_email_destinataires($flux) {
	include_spip('inc/saisies');
	include_spip('formidable_fonctions');
	include_spip('inc/formidableparticipation');
	$saisies = formidable_deserialize($flux['args']['formulaire']['saisies']);
	$traitements = formidable_deserialize($flux['args']['formulaire']['traitements']);
	$saisies = $flux['args']['saisies'];
	$traitements = $flux['args']['traitements'];
	$config_participation = $traitements['participation'] ?? [];
	if (!$config_participation) {
		return $flux;
	}
	$id_evenement = formidableparticipation_id_evenement($flux['args']['id_formulaire'], $flux['args']['id_formulaires_reponse'], $config_participation);
	$res = sql_select('participation_auteurs_supplementaires, participation_email_supplementaire', 'spip_evenements', sql_in('id_evenement', $id_evenement));
	while ($row = sql_fetch($res)) {
		$emails = explode(',', $row['participation_email_supplementaire']);
		$flux['data'] = array_merge($flux['data'], $emails);

		$emails_auteurs = sql_allfetsel('email', 'spip_auteurs', sql_in('id_auteur', $row['participation_auteurs_supplementaires']));
		foreach ($emails_auteurs as $auteurs) {
		$email = array_values($auteurs);
			$flux['data'] = array_merge($flux['data'], $email);
		}
	}
	return $flux;
}
