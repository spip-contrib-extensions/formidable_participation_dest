<?php

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Déclarer le champ extra sur les évènement pour les destinataire supplémentares
 * @param array $champs
 * @return $champs
**/
function formidable_participation_dest_declarer_champs_extras($champs = []) {
	$auteurs = [
		'saisie' => 'auteurs',
		'options' => [
			'nom' => 'participation_auteurs_supplementaires',
			'sql' => "TEXT NOT NULL DEFAULT ''",
			'versionner' => 'true',
			'multiple' => true,
			'class' => 'select2',
			'label' => _T('formidable_participation_dest:participation_auteurs_supplementaires_label'),
			'explication' => _T('formidable_participation_dest:participation_auteurs_supplementaires_explication'),
		],
	];
	$email = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'participation_email_supplementaire',
			'sql' => "TEXT NOT NULL DEFAULT ''",
			'versionner' => 'true',
			'label' => _T('formidable_participation_dest:participation_email_supplementaire_label'),
			'explication' => _T('formidable_participation_dest:participation_email_supplementaire_explication'),
		],
		'verifier' => [
			'type' => 'email'
		]
	];
	$dest = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'participation_dest',
			'label' => _T('formidable_participation_dest:participation_dest_label'),
		],
		'saisies' => [$email,$auteurs],
	];
	$champs['spip_evenements']['participation_dest'] = $dest;
	return $champs;
}
