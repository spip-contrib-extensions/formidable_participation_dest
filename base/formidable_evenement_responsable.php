<?php


/**
 * Déclarer le champ extra sur les évènement pour les responsables
 * @param array $champs
 * @return $champs
**/
function formidable_evenement_responsable_declarer_champs_extras($champs = []) {
	$auteurs = [
		'saisie' => 'auteurs',
		'options' => [
			'nom' => 'formidable_responsable_auteur',
			'sql' => "TEXT NOT NULL DEFAULT ''",
			'versionner' => 'true',
			'multiple' => true,
			'class' => 'select2',
			'label' => _T('formidable_evenement_responsable:responsable_auteur_label'),
			'explication' => _T('formidable_evenement_responsable:responsable_auteur_explication'),
		],
	];
	$email = [
		'saisie' => 'input',
		'options' => [
			'nom' => 'formidable_responsable_email',
			'sql' => "TEXT NOT NULL DEFAULT ''",
			'versionner' => 'true',
			'label' => _T('formidable_evenement_responsable:responsable_email_label'),
			'explication' => _T('formidable_evenement_responsable:responsable_email_explication'),
		],
		'verifier' => [
			'type' => 'email'
		]
	];
	$formidable_responsable = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'formidable_responsable',
			'label' => _T('formidable_evenement_responsable:responsable_label'),
		],
		'saisies' => [$auteurs, $email],
	];
	$champs['spip_evenements']['formidable_responsable'] = $formidable_responsable;
	return $champs;
}
