# Formidable evenement responsables

Extension pour Formidable + Agenda permettant d'associer un·e ou plusieurs responsables à des évènements, pour enrichir les traitements formidable.

Ce plugin remplace le plugin "formidable participation destinataire supplémentaires" qui n'est plus maintenu.
