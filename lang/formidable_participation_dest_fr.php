<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_participation_dest.git

return [

	// P
	'participation_auteurs_supplementaires_explication' => 'Lors de l’inscription à l’évènement avec Formidable, ces personnes seront prévenues. ',
	'participation_auteurs_supplementaires_label' => 'Personnes(s) à prévenir en cas d’inscription',
	'participation_dest_label' => 'Destinataires supplémentaires lors de l’inscription',
	'participation_email_supplementaire_explication' => 'Lors de l’inscription à l’évènement avec Formidable, ces emails seront prévenus. Possibilités de mettre plusieurs emails, séparés par des virgules.',
	'participation_email_supplementaire_label' => 'Email(s) à prévenir en cas d’inscription',
];
