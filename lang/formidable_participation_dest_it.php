<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_participation_dest?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// P
	'participation_auteurs_supplementaires_explication' => 'Al momento della registrazione all’evento con Formidable, questi autori verranno avvisati. E’ possibile inserire più indirizzi, separati da virgole.', # MODIF
	'participation_auteurs_supplementaires_label' => 'Autore/i da notificare in caso di registrazione', # MODIF
	'participation_dest_label' => 'Destinatari aggiuntivi durante la registrazione',
	'participation_email_supplementaire_explication' => 'Al momento della registrazione all’evento con Formidable, verrà inviata una notifica a queste email. E’ possibile inserire più indirizzi, separati da virgole.',
	'participation_email_supplementaire_label' => 'Email da notificare in caso di registrazione',
];
