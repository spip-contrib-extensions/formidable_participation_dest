<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_participation_dest.git

return [

	// F
	'formidable_evenement_responsable_description' => 'Associer un·e ou plusieurs responsables à des évènements, pour enrichir les traitements formidable. ',
	'formidable_evenement_responsable_slogan' => 'Optimiser le suivi des évènements !',
];
