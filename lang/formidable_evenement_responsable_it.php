<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable_participation_dest?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// P
	'responsable_auteur_explication' => 'Al momento della registrazione all’evento con Formidable, questi autori verranno avvisati. E’ possibile inserire più indirizzi, separati da virgole.', # MODIF
	'responsable_auteur_label' => 'Autore/i da notificare in caso di registrazione', # MODIF
	'responsable_label' => 'Destinatari aggiuntivi durante la registrazione',
	'responsable_email_explication' => 'Al momento della registrazione all’evento con Formidable, verrà inviata una notifica a queste email. E’ possibile inserire più indirizzi, separati da virgole.',
	'responsable_email_label' => 'Email da notificare in caso di registrazione',
];
