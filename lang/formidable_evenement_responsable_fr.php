<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable_participation_dest.git

return [
	//
	'destinataires_evenement_responsable_label_case' => 'Aux responsable(s) de l\'évènement choisi par l\'internaute',
	'destinataires_evenement_responsable_champ_label' => 'Champ de l\'évènement',
	// P
	'responsable_auteur_explication' => 'Ces personnes seront notamment prévenus lors de l\'inscription à l\'évènement via un formulaire Formidable',
	'responsable_auteur_label' => 'Personnes responsable(s) de l\'évènement',
	'responsable_label' => 'Responsable(s)',
	'responsable_email_explication' => 'Ces emails seront notamment prévenus lors de l\'inscription à l\'évènement via un formulaire Formidable. Possibilités de mettre plusieurs emails, séparés par des virgules.',
	'responsable_email_label' => 'Email de responsable n\'ayant pas de compte SPIP',
];
