<?php


/**
 * Lors du traitement email d'un formulaire
 * cherche les emails associés à l'évènement, et les ajoute aux destinataires
 * @param $flux
 * @return $flux
 **/
function formidable_evenement_responsable_formidable_traiter_email_destinataires(array $flux): array {
	include_spip('base/abstract_sql');
	include_spip('formidable_fonctions');
	$saisies = $flux['args']['saisies'];
	$traitements = $flux['args']['traitements'];
	$id_formulaires_reponse = $flux['args']['id_formulaires_reponse'] ?? 0;
	$id_formulaire = $flux['args']['id_formulaire'] ?? 0;

	$id_evenement = [];

	// La réponse courant est-t-elle liée à la participation à un évènement ?
	if (isset($traitements['participation'])) {
		$id_evenement = array_column(
			sql_allfetsel('id_evenement', 'spip_evenements_participants', "id_formulaires_reponse=$id_formulaires_reponse"),
			'id_evenement'
		);
	}

	foreach ($flux['args']['options']['destinataires_evenement_responsable_champ'] ?? [] as $champ) {
		if ($id_formulaires_reponse) {
			$valeur = formidable_deserialize(calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $champ, '', 'brut'));
		} else {
			$valeur = saisies_request($champ);
		}

		if (!is_array($valeur)) {
			$valeur = [$valeur];
		}

		$id_evenement = array_merge($id_evenement, $valeur);
	}

	$res = sql_select('formidable_responsable_auteur, formidable_responsable_email', 'spip_evenements', sql_in('id_evenement', $id_evenement));
	while ($row = sql_fetch($res)) {
		$emails = explode(',', $row['formidable_responsable_email']);
		$flux['data'] = array_merge($flux['data'], $emails);

		$emails_auteurs = sql_allfetsel('email', 'spip_auteurs', sql_in('id_auteur', $row['formidable_responsable_auteur']));
		foreach ($emails_auteurs as $auteurs) {
		$email = array_values($auteurs);
			$flux['data'] = array_merge($flux['data'], $email);
		}
	}
	return $flux;
}

/**
 * 1. S'assurer de placer le traitement `email` après le traitement `inscription à un évènement`
 * 2. Si pas de traitement `participation`, ajouter une option `prevenir les responsables de l'évènement` au traitement `email`
**/
function formidable_evenement_responsable_formidable_traitements(array $flux): array {
	include_spip('inc/saisies');

	// 1. Ordre des traitements
	$traitements_email_utilise = &$flux['data']['email']['utilise'];
	$traitements_email_utilise[] = 'participation';

	// 2. Options supplémentaires traitements emails

	$options_traitement_email = &$flux['data']['email']['options'];
	$saisie = [
		'saisie' => 'case',
		'options' => [
			'nom' => 'destinataires_evenement_responsable',
			'label_case' => '<:formidable_evenement_responsable:destinataires_evenement_responsable_label_case:>',
			'conteneur_class' => 'pleine_largeur',
			'afficher_si' => '@traitements_choisis@ !IN \'participation\'',
		]
	];
	$options_traitement_email = saisies_inserer_apres($options_traitement_email, $saisie, 'destinataires_selon_champ');

	$saisie = [
		'saisie' => 'champ',
		'options' => [
			'nom' => 'destinataires_evenement_responsable_champ',
			'label' => '<:formidable_evenement_responsable:destinataires_evenement_responsable_champ_label:>',
			'afficher_si' => '@traitements_choisis@ !IN \'participation\' && @traitements[email][destinataires_evenement_responsable]@',
			'obligatoire' => true,
			'defaut' => 'evenements_1',
			'type_choix' => 'checkbox',
			'forcer_type' => ['evenements'],
			'env' => true,
		]
	];


	$options_traitement_email = saisies_inserer_apres($options_traitement_email, $saisie, 'destinataires_evenement_responsable');
	return $flux;
}
