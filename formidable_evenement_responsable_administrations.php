<?php


include_spip('inc/cextras');
include_spip('base/formidable_evenement_responsable');
include_spip('base/upgrade');

function formidable_evenement_responsable_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [
		'create' => [
			['formidable_evenement_responsable_migrer_champs_sql'],
			['formidable_evenement_responsable_migrer_ordre_traitements']
		]
	];
	cextras_api_upgrade(formidable_evenement_responsable_declarer_champs_extras(), $maj['create']);
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function formidable_evenement_responsable_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(formidable_evenement_responsable_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}

/**
 * Migre les anciennes données depuis formidable_participation_destinataire_supplementaire
**/
function formidable_evenement_responsable_migrer_champs_sql() {
	include_spip('base/abstract_sql');
	$table = sql_showtable('spip_evenements', true);
	if (!isset($table['field']['participation_auteurs_supplementaires'])) {
		return;
	}
	sql_alter('TABLE spip_evenements CHANGE participation_email_supplementaire formidable_responsable_email text NOT NULL DEFAULT \'\'');
	sql_alter('TABLE spip_evenements CHANGE participation_auteurs_supplementaires formidable_responsable_auteur text NOT NULL DEFAULT \'\'');
}

/**
 * Migrer l'ordre des traitements pour que participation soit toujours après email (si les deux sont activés)
**/
function formidable_evenement_responsable_migrer_ordre_traitements() {
	include_spip('inc/formidable');
	include_spip('base/abstract_sql');
	$traitements_disponibles = array_keys(traitements_lister_disponibles());

	$res = sql_select(
		'id_formulaire, traitements',
		'spip_formulaires',
		[
			'traitements LIKE \'%email%\'',
			'traitements LIKE \'%participation%\'',
		]
	);
	while ($row = sql_fetch($res)) {
		$traitements_actuels = formidable_evenement_responsable_deserialize($row['traitements']);
		$id_formulaire = $row['id_formulaire'];
		$traitements_nouveaux = [];
		foreach ($traitements_disponibles as $t) {
			if (isset($traitements_actuels[$t])) {
				$traitements_nouveaux[$t] = $traitements_actuels[$t];
			}
		}
		$traitements = json_encode($traitements_nouveaux);
		sql_update('spip_formulaires', ['traitements' => sql_quote($traitements)], "id_formulaire = {$id_formulaire}");
	}
}


// Contournement de bug de SVP. Cf https://git.spip.net/spip-contrib-extensions/formidable_participation/-/issues/25
function formidable_evenement_responsable_deserialize($texte) {
	if (is_null($texte)) {
		$texte = '';
	}
	// Cas 1. Deja tableau
	if (is_array($texte)) {
		return $texte;
	}
	// Cas 2. Tableau serializé en json
	$tmp = json_decode($texte, true);
	if (is_array($tmp)) {
		return $tmp;
	}
	// Cas 3. Tableau serializé en PHP, si jamais ca echout on renvoie le texte
	$tmp = @unserialize($texte);
	if (is_array($tmp)) {
		return $tmp;
	}
	return $texte;
}
